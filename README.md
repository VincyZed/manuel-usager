# [Guide d'utilisateur][site]
Ce guide explique les caractéristiques du [contrôleur de serre](https://gitlab.com/chapeau-melon/controleur-serre) et comment l'utiliser en conjonction avec l'[application mobile](https://gitlab.com/chapeau-melon/app).

Vous pouvez consulter ce manuel et le télécharger en format PDF sur ce [site].

## Contribuer à ce projet
Ce projet utilise l'outil [mdBook] pour nous permettre d'écrire ce manuel d'usager en [Markdown]. Je vous invite donc à [apprendre le Markdown]() et à survoler le [manuel d'usager de mdBook][mdBook] avant de contribuer à ce projet.

### Télécharger et installer les outils de développement
En plus d'avoir besoin de [mdBook], vous aurez besoin de télécharger [mdBook LinkCheck] qui permet de vérifier que tous les liens dans [mdBook] sont valides. Il sera donc impossible pour ce manuel de pointer vers des URLs qui sont invalides.

Peu importe votre plateforme, vous pouvez télécharger les dernières versions des exécutables pour [mdBook] et [mdBook LinkCheck] ici:
* [mdBook](https://github.com/rust-lang/mdBook/releases)
* [mdBook LinkCheck](https://github.com/Michael-F-Bryan/mdbook-linkcheck/releases)

Assurez-vous de les placer dans un endroit accessible dans votre `PATH`.

### Utiliser mdBook
Pendant que vous éditez les fichiers [Markdown], vous pouvez voir en temps réel, chaque fois que vous enregistrez vos fichiers, le résultat dans votre navigateur. Pour ce faire, entrez la commande suivante dans votre terminal:
```sh
mdbook serve
```

Vous pourrez ensuite voir le résultat en allant au URL <http://localhost:3000> dans votre navigateur. Votre page de navigateur devrait se rafraîchir chaque fois que vous enregistrez les fichiers [Markdown] que vous avez édités.  
Si ce n'est pas le cas, il se peut que vous ayez fait une erreur. Vous devrez alors allez inspecter les messages d'erreurs dans votre terminal et corriger ce qui est demandé dans les fichiers du projet.

[site]: chapeau-melon.gitlab.io/manuel-usager/
[mdBook]: https://rust-lang.github.io/mdBook/
[mdBook LinkCheck]: https://github.com/Michael-F-Bryan/mdbook-linkcheck/
[Markdown]: https://commonmark.org/
