# Fonctionnalités du produit

Le contrôleur intègre donc les princpiales fonctionnalités suivantes:

- Réglage manuel des périphériques attachés au contrôleur dans la serre grâce aux interrupteurs sur le boîtier.
- Réglage et asservissement autonome des conditions dans la serre selon une consigne programmée par l'utilisateur.
- Lecture en temps réel, contrôle à distance et configuration des consignes et paramètres pour l'asservissement
  à partir de l'application Android complémentaire au contrôleur.
- Enregistrement des données sur un service de stockage infonuagique pour consultation ultérieure et statistiques.
- [...] ? 
