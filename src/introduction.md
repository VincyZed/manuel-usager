# Introduction

Ce contrôleur de serre permet d'assurer les conditions idéales favorisant un environnement propice pour la culture.
Grâce à un asservissement complet et automatique de différents paramètres comme la température et l'humidité, il
permet de simplifier la tâche demandande de l'agriculteur.

Ce document contient donc les informations nécéssaires afin d'opérer le contrôleur, de diagnostiquer
de potentiels problèmes, mais aussi des instructions par rapport aux autres éléments tels que l'application Android
et sa configuration et la manière d'accéder à l'historique des données enregistrées.
